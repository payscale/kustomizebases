apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: elasticsearch
  labels:
    app: elasticsearch-cluster
    release: elasticsearch-7.1.0
spec:
  serviceName: elasticsearch
  podManagementPolicy: Parallel
  # Number of ES nodes
  replicas: 3
  selector:
    matchLabels:
      app: elasticsearch-cluster
      release: elasticsearch-7.1.0
  template:
    metadata:
      name: elasticsearch
      annotations:
        sidecar.istio.io/inject: "false"
        ad.datadoghq.com/elasticsearch.init_configs: '[{}]'
        ad.datadoghq.com/elasticsearch.instances: '[{ "url": "http://localhost:9200" }]'
        ad.datadoghq.com/elasticsearch.logs: '[{}]'
        ad.datadoghq.com/elasticsearch.check_names: '["elasticsearch"]'
      labels:
        app: elasticsearch-cluster
        release: elasticsearch-7.1.0
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchLabels:
                app: elasticsearch-cluster
                release: elasticsearch-7.1.0
            topologyKey: kubernetes.io/hostname    
      terminationGracePeriodSeconds: 120
      containers:
      - name: elasticsearch
        image: docker.elastic.co/elasticsearch/elasticsearch:7.1.0
        resources:
          limits:
            cpu: 1000m # override these in a kustomize patch
            memory: 8Gi # override these in a kustomize patch
          requests:
            cpu: 250m # override these in a kustomize patch
            memory: 2Gi # override these in a kustomize patch
        ports:
        - containerPort: 9200
          name: http
        - containerPort: 9300
          name: transport
        envFrom:
        - configMapRef:
            name: elasticsearch-envvar-config       
        env:
          - name: node.name
            valueFrom:
              fieldRef:
                fieldPath: metadata.name
          - name: cluster.initial_master_nodes # the initial master node(s) must match a node name or else the cluster won't form
            value: $(ELASTICPODNAME)-0
          - name: discovery.seed_hosts
            value: elasticsearch-discovery
          - name: ES_JAVA_OPTS
            value: -Xmx1g -Xms1g
          - name: node.data
            value: "true"
          - name: node.ingest
            value: "true"
          - name: node.master
            value: "true"
          - name: ES_PATH_CONF
            value: /usr/share/elasticsearch/config
        readinessProbe:
          exec:
            command:
            - sh
            - -c
            - |
              #!/usr/bin/env bash -e
              # If the node is starting up wait for the cluster to be ready (request params: 'wait_for_status=green&timeout=1s' )
              # Once it has started only check that the node itself is responding
              START_FILE=/tmp/.es_start_file

              http () {
                  local path="${1}"
                  if [ -n "${ELASTIC_USERNAME}" ] && [ -n "${ELASTIC_PASSWORD}" ]; then
                    BASIC_AUTH="-u ${ELASTIC_USERNAME}:${ELASTIC_PASSWORD}"
                  else
                    BASIC_AUTH=''
                  fi
                  curl -XGET -s -k --fail ${BASIC_AUTH} http://127.0.0.1:9200${path}
              }

              if [ -f "${START_FILE}" ]; then
                  echo 'Elasticsearch is already running, lets check the node is healthy'
                  http "/"
              else
                  echo 'Waiting for elasticsearch cluster to become cluster to be ready (request params: "wait_for_status=green&timeout=1s" )'
                  if http "/_cluster/health?wait_for_status=green&timeout=1s" ; then
                      touch ${START_FILE}
                      exit 0
                  else
                      echo 'Cluster is not yet ready (request params: "wait_for_status=green&timeout=1s" )'
                      exit 1
                  fi
              fi
          failureThreshold: 3
          initialDelaySeconds: 10
          periodSeconds: 10
          successThreshold: 3
          timeoutSeconds: 5            
        volumeMounts:
        - name: elasticsearch-data-claim
          mountPath: /usr/share/elasticsearch/data
        - name: elasticsearch-file-config
          mountPath: /usr/share/elasticsearch/config/elasticsearch.yml
          subPath: elasticsearch.yml
        - name: elasticsearch-file-config
          mountPath: /usr/share/elasticsearch/config/role_mapping.yml
          subPath: role_mapping.yml
      volumes:
      - name: elasticsearch-file-config
        configMap:
          name: elasticsearch-file-config
      initContainers:
      - command:
        - sysctl
        - -w
        - vm.max_map_count=262144          
        image: docker.elastic.co/elasticsearch/elasticsearch:7.1.0
        name: configure-sysctl
        resources: {}
        securityContext:
          privileged: true
          runAsUser: 0       
      securityContext:
        fsGroup: 1000

  volumeClaimTemplates:
  - metadata:
      name: elasticsearch-data-claim
    spec:
      accessModes: [ "ReadWriteOnce" ]
      storageClassName: azure-disk-premium-retain
      resources:
        requests:
          storage: 10Gi
---
apiVersion: policy/v1beta1
kind: PodDisruptionBudget
metadata:
  name: elasticsearch-7.1.0-pdb
spec:
  maxUnavailable: 1
  selector:
    matchLabels:
      app: elasticsearch-cluster
      release: elasticsearch-7.1.0
