# Elastic Search 7.1.0 monocluster

## Overview

The resources in this folder are used to provision an ElasticSearch 7.1.0 cluster where each instance is running all three roles (master, data, and ingest). This configuration should be sufficient for most deployments. It is recommended to provision nodes with a single type when the node count is greater than 10.

## Files

1. /resources - the resources for ElastisSearch 
2. /sample - example of usage

## Naming note

Using this base as-is will result in an ElasticSearch cluster with a default name of "elasticsearch". This means that if you use this as is, you could collide with another ElasticSearch cluster in your namespace. This will work, but may lead to unexpected behaviors. It is recommended to use labels and name prefix/suffixs to avoid this.

## Usage

In most kustomize projects, you should have a folder structure like the following:

/base-A
/base-B
/overlays/environmentA
/overlays/environmentB

It is recommeded to pull in this resource into a base named external. This base should contain a kustomize.yaml file that pull in this resource using the kustomize functionality that allows for using a git repo as a remote. The kustomize should look similar to the following:

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

bases:
- git@bitbucket.org:org/kustomizebases.git//elasticsearch/v7.1.0/monocluster/resources?ref=master

namePrefix: esforme-

commonLabels:
  esinstance: esforme
```  

The ```namePrefix``` and ```commonLabels``` are there to allow for multiple ES clusters within a given k8s namespace. Without these, the names in the base will be used, which will lead to problems if a second usage of this base is deployed, as the label selectors will overlap.

The result of this kustomize is an ES cluster that is ready to run with a minimal configuration.

## ElasticSearch Configuration

The base resource contains two configMapGenerators: ```elasticsearch-envvar-config``` and ```elasticsearch-file-config```

```elasticsearch-envvar-config``` contains values that will be passed to the pod as environment variables, and is used to set or override settings, either default or via configuration file. The base resource contains the following:

1. ```network.host=0.0.0.0```
2. ```http.port=9200```
3. ```cluster.name=escluster```

The cluster name should be overridden in the overlay kustomization.yaml. 

```elasticsearch-file-config``` contains two keys: ```elasticsearch.yml``` and ```role_mapping.yml```. Each key represents a yaml file that will be mounted in the pod under the folder ```/usr/share/elasticsearch/config```.

Common settings for ```elasticsearch.yml``` include CORS and security settings. See sample for examples.

## Kubernetes configuration

If you would like to configure the kubernetes aspect of the ES cluster, such as cluster size, resource usage, disk size, etc, you should create a file containing the overrides in a kustomization supported format.

Some common settings

1. replicas - the number of ES instances to run
2. cpu and memory limits
3. volumeClaimTemplates size

## Istio interaction

If you are not using Istio **or** do not want to access your ElasticSearch cluster from outside k8s (you may be using port forwarding instead) you can skip this section.

In the overlay folder, you should include the following files:

1. elasticsearch-istio-gateway.yaml
1. elasticsearch-istio-virtualservice.yaml

At the current time, there is a kustomize bug (https://github.com/kubernetes-sigs/kustomize/issues/1082) that prevents the namePrefix from being correctly applied. As a result of this, any Istio objects will need to be created with the *final* name, not the base name. When this issue is resolved, this repo will be updated to use the fixed approach.

In the VirtualService, you should update the destination host to reflect the final value for the ```elasticsearch``` service.
