
$scriptPath = Split-Path $MyInvocation.MyCommand.Path
Write-Host "scriptPath: $scriptPath"

Write-Host "building deployment YAML"
$outputFile = "$scriptPath/deployment.yaml"
$deployYaml = kustomize build ./overlays/development 
if ($?) {
    Set-Content -Path $outputFile -Value $deployYaml -Force
    Write-Host "Saved deployment YAML as $outputFile"
}
else {
    exit 3
}
