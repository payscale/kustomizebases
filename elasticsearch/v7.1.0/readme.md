**Elastic Search 7.1.0**

**monocluster**

The resources in this folder are used to provision an ElasticSearch cluster where each instance is running all three roles (master, data, and ingest). This configuration should be sufficient for most deployments. It is recommended to provision nodes with a single type when the node count is greater than 10.